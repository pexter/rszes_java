package javathread.example2;

class Sum {

    private int sum;

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
}

class Summation implements Runnable {

    private int upper;
    private int lower;
    private Sum sumValue;

    public Summation(int lower, int upper, Sum sumValue) {
        this.lower = lower;
        this.upper = upper;
        this.sumValue = sumValue;
    }

    public void run() {
        int sum = 0;
        for (int i = lower; i <= upper; i++) {
            sum += i;
        }
        synchronized (sumValue) {
            sumValue.setSum(sumValue.getSum() + sum);
        }
        //pokusajte da sumiranje radite bez synchronized bloka
        //rezultat moze biti dobar, ali se pojavljuju i pogresni rezultati u kojima se
        //ignorise racunanje prve, odnosno druge niti
        //sumValue.setSum(sumValue.getSum() + sum);
    }
}

public class JavaThreads {

    public static void main(String[] args) {
        System.out.println("Running example with two threads!");
        if (args.length > 0) {
            if (Integer.parseInt(args[0]) < 0) {
                System.err.println(args[0] + " must be >= 0.");
            } else {
                Sum sumObject = new Sum();
                int upper = Integer.parseInt(args[0]);
                int lower = upper / 2 - 1;
                Thread thrd1 = new Thread(new Summation(0, lower, sumObject));
                Thread thrd2 = new Thread(new Summation(lower + 1, upper, sumObject));
                thrd1.start();
                thrd2.start();
                try {
                    thrd1.join();
                    thrd2.join();
                    System.out.println("The sum from 0 to " + upper + " is "
                            + sumObject.getSum());
                } catch (InterruptedException ie) {
                }
            }
        } else {
            System.err.println("Usage: Summation <integer value>");
        }
    }
}
