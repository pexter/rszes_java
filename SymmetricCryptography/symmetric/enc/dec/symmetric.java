package symmetric.enc.dec;
  
import java.security.SecureRandom;
import java.util.Scanner;
  
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.xml.bind.DatatypeConverter;
  
// Klasa koja implementira simetricnu enkripciju
public class symmetric {
  
    private static final String AES = "AES";
  
    // Block cipher(CBC mode) - tzv Blok sifra kod koje se originalna poruka sifruje po grupama (blokovima)
    private static final String AES_CIPHER_ALGORITHM = "AES/CBC/PKCS5PADDING";
  
    private static Scanner message;
  
    // Funkcija koja kreira skriveni kljuc
    public static SecretKey createAESKey() throws Exception {
        SecureRandom securerandom = new SecureRandom();
        //prilikom pravljenja kljuca navodi se koji se algoritam koristi
        KeyGenerator keygenerator = KeyGenerator.getInstance(AES);
  
        //duzina kljuca se navodi prilikom pozivanja init funkcije
        //ovde koristimo duzinu 128 bita (za 256 bita je potrebno instalirati 
        //dodatne pakete)
        keygenerator.init(128, securerandom);
        
        SecretKey key = keygenerator.generateKey();
  
        return key;
    }
  
    // Funkcija koja kreira inicijalizacioni vektor - on je potreban kako bi se obezbedilo
    // tzv inicijalno stanje. Kod sifrovanja u blokovima, isti blokovi otvorenog teksta se
    //sifruju u iste blokove kriptovanog teksta, sto se moze spreciti inicijalizacionim vektorom
    // Inicijalizacioni vektor se ne smatra tajnom u kriptografskom sistemu, tj. moze se nekriptovan slati 
    public static byte[] createInitializationVector()
    {

        //velicina inicijalizacionog vektora
        byte[] initializationVector = new byte[16];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(initializationVector);
        return initializationVector;
    }
  
    //Funkcija koja prima otvoreni tekst, kljuc i inicijalizacioni vektor i 
    //generise sifrat (cipher text)
    public static byte[] do_AESEncryption(String plainText, SecretKey secretKey, byte[] initializationVector) throws Exception{
        //klasa Cipher se koristi za enkripciju/dekripciju, prilikom kreiranja navodi se koji algoritam se koristi
        Cipher cipher = Cipher.getInstance(AES_CIPHER_ALGORITHM);
        
        //IvParameterSpec se kreira koristeci inicijalizacioni vektor a potreban je za inicijalizaciju cipher objekta
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initializationVector);
  
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec);
  
        //metoda doFinal nakon sto se inicijalizuje metodom init, vrsi enkripciju otvorenog teksta
        return cipher.doFinal(plainText.getBytes());
    }

    //Funkcija koja prima sifrat (kriptovan tekst), kljuc i inicijalizacioni vektor i vraca dekriptovani tekst
    //generise sifrat (cipher text)
    public static String do_AESDecryption(byte[] cipherText, SecretKey secretKey, byte[] initializationVector) throws Exception{
        Cipher cipher = Cipher.getInstance(AES_CIPHER_ALGORITHM);
  
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initializationVector);
  
        cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);
  
        //ista metoda doFinal se koriti i za dekripciju
        byte[] result = cipher.doFinal(cipherText);
  
        return new String(result);
    }
  
    // Main metoda 
    public static void main(String args[]) throws Exception{
        //generisi kljuc za enkripciju/dekripciju
        SecretKey symmetricKey = createAESKey();
  
        //prikazi kljuc (prethodno ga konvertuj koriscenjem DatatypeConverter klase kako bi se mogao prikazati u formi stringa
        System.out.println("Kljuc koji se koristi za simetricnu enkripciju :" + DatatypeConverter.printHexBinary(symmetricKey.getEncoded()));
  
        //Napravi inicijalizacioni vektor
        byte[] initializationVector = createInitializationVector();
  
        //Otvoreni tekst, poruka koja se kriptuje
        //Kod simetricne enkripcije/dekripcije ne postoji ogranicenje na velicinu poruke koja se kriptuje/dekriptuje
        //String plainText = "Ovo je poruka koju je potrebno kriptovati.";
        String plainText = "RSA (Rivest–Shamir–Adleman) is a public-key cryptosystem that is widely used "
                + "for secure data transmission. It is also one of the oldest. The acronym RSA comes from "
                + "the surnames of Ron Rivest, Adi Shamir, and Leonard Adleman, who publicly described the "
                + "algorithm in 1977. An equivalent system was developed secretly, in 1973 at GCHQ "
                + "(the British signals intelligence agency), by the English mathematician Clifford Cocks. "
                + "That system was declassified in 1997.";
        System.out.println("Originalna poruka je: " + plainText);
  
        // Enkripcija poruke
        byte[] cipherText = do_AESEncryption(plainText, symmetricKey, initializationVector);
  
        System.out.println("Sifrat, odnosno kriptovana poruka je: " + DatatypeConverter.printHexBinary(cipherText));
  
        // Dekriptovanje poruke
        String decryptedText = do_AESDecryption(cipherText, symmetricKey, initializationVector);
  
        System.out.println("Desifrovana poruka je: " + decryptedText);
    }
}