package quoteclient;

import java.io.*;
import java.net.*;
 
/**
 * This program demonstrates how to implement a UDP client program.
 *
 *
 * @author www.codejava.net
 */
public class QuoteClient {
 
    public static void main(String[] args) {
        if (args.length < 2) {
            //Poziva se sa dva argumenta, npr djxmmx.net 17 ako se poziva postojeci server ili
            //localhost portnumber ako se koristi nas
            System.out.println("Syntax: QuoteClient <hostname> <port>");
            return;
        }
 
        String hostname = args[0];
        int port = Integer.parseInt(args[1]);
 
        try {
            //na osnovu hostname odredi addresuu
            InetAddress address = InetAddress.getByName(hostname);
            //kreiraj UDP socket za klijenta, pri čemu će ip adresa i port na koji se šalje biti naknadno definisani
            DatagramSocket socket = new DatagramSocket();
 
            while (true) {
 
                //DatagramPacket se koristi za svaki paket koji se salje/prima
                //Prilikom kreiranja ovog objekta prosledjuje se adresa i port
                DatagramPacket request = new DatagramPacket(new byte[1], 1, address, port);
                socket.send(request);
 
                byte[] buffer = new byte[512];
                DatagramPacket response = new DatagramPacket(buffer, buffer.length);
                socket.receive(response);
 
                //Ono sto se dobije samo prebaci u string kako bi se ispisao
                String quote = new String(buffer, 0, response.getLength());
 
                System.out.println(quote);
                System.out.println();
 
                //sacekaj 10s da bi ponovio zahtev
                Thread.sleep(10000);
            }
 
        } catch (SocketTimeoutException ex) {
            System.out.println("Timeout error: " + ex.getMessage());
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Client error: " + ex.getMessage());
            ex.printStackTrace();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}