/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package slidingpuzzle;

/**
 *
 * @author pedja
 */
public class Box {
    private String label;
    private int x;
    private int y;
    private int maxX;
    private int maxY;
    
    public Box(int x, int y, int maxX, int maxY, String label){
        this.x = x;
        this.y = y;
        this.maxX = maxX;
        this.maxY = maxY;
        this.label = label;
    }

    public int getMaxX() {
        return maxX;
    }

    public void setMaxX(int maxX) {
        this.maxX = maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public void setMaxY(int maxY) {
        this.maxY = maxY;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    public boolean moveLeft(){
        if (this.x > 0){
            this.x = this.x - 1;
            return true;
        }
        else{
            return false;
        }
    }
    public boolean moveRight(){
        if (this.x < this.maxX - 1){
            this.x = this.x + 1;
            return true;
        }
        else{
            return false;
        }
    }
    public boolean moveUp(){
        if (this.y > 0){
            this.y = this.y - 1;
            return true;
        }
        else{
            return false;
        }
    }
    public boolean moveDown(){
        if (this.y < maxY - 1){
            this.y = this.y + 1;
            return true;
        }
        else{
            return false;
        }
    }
            
}
