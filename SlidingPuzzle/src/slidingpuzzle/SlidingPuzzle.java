/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package slidingpuzzle;

import java.util.ArrayList;

/**
 *
 * @author pedja
 */
public class SlidingPuzzle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        int[] refLabels = {0,1,2,3};
        int[] refLabels = {0,1,2,3,4,5,6,7,8};
//        int[] initLabels = {3,1,2,0};
//        int[] initLabels = {1,3,2,0};
        int[] initLabels = {1,3,2,0,7,8,6,5,4};
        Board refBoard = new Board(refLabels, 3, 3);
        Board initBoard = new Board(initLabels, 3, 3);
        ArrayList<Board> fifo = new ArrayList<>();
        fifo.add(initBoard);
        boolean done = false;
        int counter = 0;
        while (!done){
            Board board = fifo.remove(0);
            if (board.equals(refBoard)){
                printoutSolution(board);
                done = true;
            }
            else{
                String blankPos = board.findBlank();
                String left = board.findBoxLeftCoords(blankPos);
                if (!left.equals("")){
                    Board child = new Board(board);
                    child.moveBox(left, "right");
                    if (board.getParent() != null){
                        if (!child.equals(board.getParent())){
//                            child.printOutBox();
                            fifo.add(child);
                        }
                    }else{
                            fifo.add(child);
                    }
                }
                String right = board.findBoxRightCoords(blankPos);
                if (!right.equals("")){
                    Board child = new Board(board);
                    child.moveBox(right, "left");
                    if (board.getParent() != null){
                        if (!child.equals(board.getParent())){
//                            child.printOutBox();
                            fifo.add(child);
                        }
                    }else{
                            fifo.add(child);
                    }
                }
                String up = board.findBoxUpCoords(blankPos);
                if (!up.equals("")){
                    Board child = new Board(board);
                    child.moveBox(up, "down");
                    if (board.getParent() != null){
                        if (!child.equals(board.getParent())){
//                            child.printOutBox();
                            fifo.add(child);
                        }
                    }else{
                            fifo.add(child);
                    }
                }
                String down = board.findBoxDownCoords(blankPos);
                if (!down.equals("")){
                    Board child = new Board(board);
                    child.moveBox(down, "up");
                    if (board.getParent() != null){
                        if (!child.equals(board.getParent())){
//                            child.printOutBox();
                            fifo.add(child);
                        }
                    }else{
                            fifo.add(child);
                    }
                }
                counter++;
                if (counter == 1000000){
                    System.out.println("Could not find solution");
                    done = true;
                }
            }
        }
    }

    private static void printoutSolution(Board board) {
        ArrayList<Board> printoutlist = new ArrayList<>();
        printoutlist.add(board);
        while (board.getParent() != null){
            board = board.getParent();
            printoutlist.add(0, board);
        }
        for(Board brd : printoutlist){
            brd.printOutBox();
        }
    }
}
