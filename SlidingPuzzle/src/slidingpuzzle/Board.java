/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package slidingpuzzle;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author pedja
 */
public class Board {
    HashMap<String,Box> boxes;

    public Board getParent() {
        return parent;
    }

    public void setParent(Board parent) {
        this.parent = parent;
    }
//    ArrayList<Board> children;
    private Board parent;
    int maxX;
    int maxY;
    
    public Board(Board parent){
        this.parent = parent;
        this.boxes = new HashMap<>();
        for (String key : parent.boxes.keySet()){
            this.boxes.put(key, parent.boxes.get(key));
        }
//        this.children = null;
        this.maxX = parent.maxX;
        this.maxY = parent.maxY;
//        if (parent.children == null){
//            parent.children = new ArrayList<>();
//        }
//        parent.children.add(this);
    }
    
    public Board(int[] intLabels, int maxX, int maxY){
        if (intLabels.length != maxX * maxY){
            //TODO: randomize elements
        }
        this.maxX = maxX;
        this.maxY = maxY;
        int index = 0;
        this.boxes = new HashMap<>();
        for (int i = 0; i < this.maxX; i++){
            for(int j = 0; j < this.maxY; j++){
                   String str = intLabels[index] == 0 ? "blank" : Integer.toString(intLabels[index]);
                   boxes.put(coordsToString(i,j),new Box(i,j,maxX,maxY,str));
                   index++;
            }
        }
//        children = null;
        parent = null;
        
    }
    
    final String coordsToString(int x, int y){
        return "(" + x + "," + y + ")";
    }
    
    int strintToX(String coords){
        String trimmed = coords.substring(1, coords.length()-1);
        int x = Integer.parseInt(trimmed.split(",")[0]);
        return x;
    }
    int strintToY(String coords){
        String trimmed = coords.substring(1, coords.length()-1);
        int y = Integer.parseInt(trimmed.split(",")[1]);
        return y;
    }
    void moveBox(String coords, String dir){
        Box toMove = this.boxes.get(coords);
        Box temp = null;
        String where = null;
        switch (dir){
            case "right": 
                where = findBoxRightCoords(coords);
                this.boxes.get(coords).moveRight();
                this.boxes.get(where).moveLeft();
                break;
            case "left": 
                where = findBoxLeftCoords(coords);
                this.boxes.get(coords).moveLeft();
                this.boxes.get(where).moveRight();
                break;
            case "up": 
                where = findBoxUpCoords(coords);
                this.boxes.get(coords).moveUp();
                this.boxes.get(where).moveDown();
                break;
            case "down": 
                where = findBoxDownCoords(coords);
                this.boxes.get(coords).moveDown();
                this.boxes.get(where).moveUp();
        }
        if (!where.equals("")){
            temp = this.boxes.get(where);
            this.boxes.put(where, toMove);
            this.boxes.put(coords, temp);       
        }
    }
    String findBlank(){
        for (String key : this.boxes.keySet()){
            if (this.boxes.get(key).getLabel().equals("blank")){
                return key;
            }
        }
        return null;
    }

    public String findBoxRightCoords(String coords) {
        int x = this.strintToX(coords);
        int y = this.strintToY(coords);
        if (y < this.maxY - 1){
            y++;
        }
        else{
            return "";
        }
        return this.coordsToString(x, y);
    }

    public String findBoxLeftCoords(String coords) {
        int x = this.strintToX(coords);
        int y = this.strintToY(coords);
        if (y > 0){
            y--;
        }
        else{
            return "";
        }
        return this.coordsToString(x, y);
    }

    public String findBoxUpCoords(String coords) {
        int x = this.strintToX(coords);
        int y = this.strintToY(coords);
        if (x > 0){
            x--;
        }
        else{
            return "";
        }
        return this.coordsToString(x, y);
    }

    public String findBoxDownCoords(String coords) {
        int x = this.strintToX(coords);
        int y = this.strintToY(coords);
        if (x < this.maxX - 1){
            x++;
        }
        else{
            return "";
        }
        return this.coordsToString(x, y);
    }
    
    @Override
    public boolean equals(Object obj){
        if (obj instanceof Board){
            Board brd = (Board) obj;
            for (String key : this.boxes.keySet()){
                if (!this.boxes.get(key).getLabel().equals(brd.boxes.get(key).getLabel())){
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    

    public void printOutBox(){
        for (int i = 0; i < this.maxX; i++){
            for (int j = 0; j < this.maxY; j++){
                String key = this.coordsToString(i, j);
                String label = this.boxes.get(key).getLabel();
                System.out.print(" " + (label.equals("blank")?"0":label) +" ");
                if (j < this.maxY - 1)
                    System.out.print("|");
            }
            System.out.println("\n----------");
        }
        System.out.println("");
        System.out.println("");
    }
}
