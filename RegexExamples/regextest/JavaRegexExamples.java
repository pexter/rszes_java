/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regextest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JavaRegexExamples {

    public static final String BLANK_REPLACE_TEST = "Student sam master studija , imam "
            + "odlican prosek .";
    public static final String DOUBLE_WORD_TEST = "Student sam master master studija, imam "
            + "odlican prosek prosek.";
    public static final String VALID_EMAIL_ADDRESS = "t_pedja@uns.ac.rs";
    public static final String INVALID_EMAIL_ADDRESS = "teodorovic.predrag@gmail.c0m";
    public static final String EMAIL_ADDRESS_TEST = "Moja fakultetska email adresa je "
            + "t_pedja@uns.ac.rs, a privatna teodorovic.predrag@gmail.com. ";

    private static void simpleMatch() {
        String regexPattern = "\\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}\\b";
        System.out.print("Provera da li je " + VALID_EMAIL_ADDRESS + " validna email adresa: ");
        System.out.println(VALID_EMAIL_ADDRESS.matches(regexPattern)?" jeste":" nije");
        System.out.print("Provera da li je " + INVALID_EMAIL_ADDRESS + " validna email adresa: ");
        System.out.println(INVALID_EMAIL_ADDRESS.matches(regexPattern)?" jeste":" nije");
        
    }
    
    public static void replaceExamples(){
        //regex sablon je karekter \w iza kojeg sledi barem jedan whitespace karakter
        //pracen tackom ili zarezom 
        String pattern = "(\\w)(\\s+)([.,])";
        String replaceDoubleWordsPattern = "\\b(\\w+)\\s+\\1\\b";
        System.out.println("");
        System.out.println("Izbacivanje dvostrukih reci iz teksta:");
        //replaceAll koristi regularne izraze, replace ne
        String doubleWordsCorrected = DOUBLE_WORD_TEST.replaceAll(replaceDoubleWordsPattern, "$1");
        System.out.println(DOUBLE_WORD_TEST + " --> " + doubleWordsCorrected);
        System.out.println("Izbacivanje whitespace karaktera pre tacke ili zareza:");
        String noBlanksBeforeDotComma = BLANK_REPLACE_TEST.replaceAll(pattern, "$1$3");
        System.out.println(BLANK_REPLACE_TEST + " --> " + noBlanksBeforeDotComma);
        //replaceFirst takodje koristi regularne izraze, zamenjuje samo prvo pojavljivanje
        System.out.println("Izbacivanje samo prvog slucaja pojave whitespace karaktera pre tacke ili zareza:");
        String noBlanksBeforeDotCommaFirst = BLANK_REPLACE_TEST.replaceFirst(pattern, "$1$3");
        System.out.println(BLANK_REPLACE_TEST + " --> " + noBlanksBeforeDotCommaFirst);
    }
    
    public static void patternMatch(){
        String regexPattern = "\\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}\\b";
        Pattern pattern = Pattern.compile(regexPattern);
        // ako zelimo da ne vodimo racuna o velikim i malim slovima (case-insensitive)
        // Pattern pattern = Pattern.compile(regexPattern, Pattern.CASE_INSENSITIVE);
        System.out.println("");
        Matcher matcher = pattern.matcher(EMAIL_ADDRESS_TEST);
        System.out.println("Pronalazenje svih email adresa u datom tekstu:");
        while (matcher.find()) {
            System.out.print("Start index: " + matcher.start());
            System.out.print(" End index: " + matcher.end() + " email adresa: ");
            System.out.println(matcher.group());
        }
    }
    
    public static void main(String[] args) {
        simpleMatch();
        replaceExamples();
        patternMatch();
    }

}
