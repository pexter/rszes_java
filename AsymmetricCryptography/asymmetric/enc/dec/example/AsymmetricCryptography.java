package asymmetric.enc.dec.example;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import java.util.Base64;

public class AsymmetricCryptography {
    //kao i kod simetricne enkripcije, klasa Cipher se koristi
    private Cipher cipher;

    //u konstruktoru samo navodimo koji tip enkripcije se koristi
    public AsymmetricCryptography() throws NoSuchAlgorithmException, NoSuchPaddingException {
        this.cipher = Cipher.getInstance("RSA");
    }

    //metoda koja vraca privatni kljuc iz datoteke
    public PrivateKey getPrivate(String filename) throws Exception {
        byte[] keyBytes = Files.readAllBytes(new File(filename).toPath());
        //PKCS8 je standard za cuvanje informacija o privatnom kljucu
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    //metoda koja vraca javni kljuc iz datoteke
    public PublicKey getPublic(String filename) throws Exception {
        byte[] keyBytes = Files.readAllBytes(new File(filename).toPath());
        //X509 je standard koji definise format sertifikata javnih kljuceva
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

    //metoda koja se koristi da sifruje ulaz input, sacuva rezultate u datoteci output
    public void encryptFile(byte[] input, File output, Key key) throws IOException, GeneralSecurityException {
        this.cipher.init(Cipher.ENCRYPT_MODE, key);
        writeToFile(output, this.cipher.doFinal(input));
    }

    //metoda koja se koristi da desifruje ulaz input, sacuva rezultate u datoteci output
    public void decryptFile(byte[] input, File output, Key key) throws IOException, GeneralSecurityException {
        this.cipher.init(Cipher.DECRYPT_MODE, key);
        writeToFile(output, this.cipher.doFinal(input));
    }

    private void writeToFile(File output, byte[] toWrite) throws IllegalBlockSizeException, BadPaddingException, IOException {
        FileOutputStream fos = new FileOutputStream(output);
        fos.write(toWrite);
        fos.flush();
        fos.close();
    }

    //metoda koja enkriptuje tekst msg koristeci kljuc Key
    public String encryptText(String msg, Key key) throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        this.cipher.init(Cipher.ENCRYPT_MODE, key);
        //Da bi se sifrovani podaci mogli prikazati u formi teksta koristi se Base64 kodovanje
        //ovo kodovanje binarne podatke prebacuje u ASCII
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(cipher.doFinal(msg.getBytes("UTF-8")));
    }

    //metoda koja dekriptuje tekst msg koristeci kljuc Key
    public String decryptText(String msg, Key key) throws InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
        this.cipher.init(Cipher.DECRYPT_MODE, key);
        //Slicno kao od enkripcije, koristi se Base64 dekoder koji ASCII tekst konvertuje nazad u binarne podatke
        Base64.Decoder decoder = Base64.getDecoder();
        return new String(cipher.doFinal(decoder.decode(msg)), "UTF-8");
    }

    //metoda koja cita podatke iz datoteke i vraca ih u nizu bajtova
    public byte[] getFileInBytes(File f) throws IOException {
        FileInputStream fis = new FileInputStream(f);
        byte[] fbytes = new byte[(int) f.length()];
        fis.read(fbytes);
        fis.close();
        return fbytes;
    }

    //Main metoda
    public static void main(String[] args) throws Exception {
        AsymmetricCryptography ac = new AsymmetricCryptography();
        //najpre preuzmi privatni i javni kljuc koji ce se koristi za enkripciju/dekripciju
        PrivateKey privateKey = ac.getPrivate("KeyPair/privateKey");
        PublicKey publicKey = ac.getPublic("KeyPair/publicKey");

        String msg = "Cryptography is fun!";
        //moze enkripcija sa privatnim, obicno se to koristi kod digitalnog potpisa
        String encrypted_msg = ac.encryptText(msg, privateKey);
        String decrypted_msg = ac.decryptText(encrypted_msg, publicKey);
        System.out.println("Original Message: " + msg + "\nEncrypted Message: " + encrypted_msg + "\nDecrypted Message: " + decrypted_msg);

        msg = "Cryptography is even more fun when sending encrypted messages!";
        //standardna upotreba kljuceva, otvoren tekst se kriptuje javnim kljucem, a dekriptuje privatnim
        encrypted_msg = ac.encryptText(msg, publicKey);
        decrypted_msg = ac.decryptText(encrypted_msg, privateKey);
        System.out.println("Original Message: " + msg + "\nEncrypted Message: " + encrypted_msg + "\nDecrypted Message: " + decrypted_msg);

        
        /*
        ako postoji datoteka text.txt, uradi na njoj enkripciju/dekripciju
        Maksimalna velicina datoteke je velicina_kljuca(u bitima) / 8 - 11 (bajtova za padding)
        u ovom slucaju 1024/8-11 = 117 bajtova
        U slucaju da treba poslati vise podataka, procedura je sledeca:
            1. Generisi par skriveni-javni kljuc za RSA i posalji javni drugoj strani
            2. Druga strana primi javni RSA kljuc i generise tajni kljuc za AES
            3. Kriptuje tajni AES kljuc pomocu javnog RSA kljuca i salje ga drugoj strani
            4. Druga strana koristi tajni RSA kljuc koji je kod nje da dekoduje tajni AES kljuc
            5. Koristi se tajni AES kljuc za dalju komunikaciju.
        */
        
        if (new File("KeyPair/text.txt").exists()) {
            ac.encryptFile(ac.getFileInBytes(new File("KeyPair/text.txt")), new File("KeyPair/text_encrypted.txt"), privateKey);
            ac.decryptFile(ac.getFileInBytes(new File("KeyPair/text_encrypted.txt")), new File("KeyPair/text_decrypted.txt"), publicKey);
        } else {
            System.out.println("Create a file text.txt under folder KeyPair");
        }
    }
}
