/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quoteserver;

import java.io.*;
import java.net.*;
import java.util.*;
 
/**
 * This program demonstrates how to implement a UDP server program.
 *
 *
 * @author www.codejava.net
 */
public class QuoteServer {
    private DatagramSocket socket;
    private List<String> listQuotes = new ArrayList<String>();
    private Random random;
 
    public QuoteServer(int port) throws SocketException {
        //prilikom kreiranja DatagramSocket-a navedi samo port-tako se kreira server koji slusa na datom portu
        socket = new DatagramSocket(port);
        //random koristimo za odabir jedne od vise linija iz datoteke 
        random = new Random();
    }
 
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Syntax: QuoteServer <file> <port>");
            return;
        }
 
        String quoteFile = args[0];
        int port = Integer.parseInt(args[1]);
 
        try {
            QuoteServer server = new QuoteServer(port);
            server.loadQuotesFromFile(quoteFile);
            //metoda service implementira petlju u kojoj se salju odgovori klijentima
            server.service();
        } catch (SocketException ex) {
            System.out.println("Socket error: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("I/O error: " + ex.getMessage());
        }
    }
 
    private void service() throws IOException {
        while (true) {
            //primi jedan po jedan paket, server ce se blokirati prilikom poziva metode receive
            //sve dok ne pristigne zahtev od strane klijenta
            DatagramPacket request = new DatagramPacket(new byte[1], 1);
            socket.receive(request);
 
            //kada stigne zahtev, preuzmi iz datoteke random liniju
            String quote = getRandomQuote();
            byte[] buffer = quote.getBytes();
 
            //prepoznaj adresu i port servera koji se javio
            InetAddress clientAddress = request.getAddress();
            int clientPort = request.getPort();
 
            //kreiraj DatagramPacket sa adresom i portom odredista i posalji poruku
            DatagramPacket response = new DatagramPacket(buffer, buffer.length, clientAddress, clientPort);
            socket.send(response);
        }
    }
 
    //Cita sve linije iz date datoteke i smesta ih u listu stringova kako bi se kasnije slali klijentima
    private void loadQuotesFromFile(String quoteFile) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(quoteFile));
        String aQuote;
 
        while ((aQuote = reader.readLine()) != null) {
            listQuotes.add(aQuote);
        }
 
        reader.close();
    }
 
    //generise random index u zavisnosti koliko je linija teksta bilo upisano u datoteci sa tekstom
    //i na osnovu tog indeksa vraca odgovorajucu liniju 
    private String getRandomQuote() {
        int randomIndex = random.nextInt(listQuotes.size());
        String randomQuote = listQuotes.get(randomIndex);
        return randomQuote;
    }
}